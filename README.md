# Stylish IDE Toolkit

A set of classes, routines and wrappers to other libraries to ease and speed-up the IDE creation.

Created by j-a-s-d

Original project could be found here: https://sourceforge.net/projects/stylishide/